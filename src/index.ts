export const projectomondo = {
  /**
   * This includes data related functions
   * @type {Object}
   */
  data: {
    /**
     * This includes object related functions
     * @type {Object}
     */
    obj: {
      /**
       * Use this function to join multiple objects together
       * @param {Object[]} objs - these are the objects to join
       * @returns {Object} - this returns an object which is all the objects together
       *
       * @example
       *
       *  join({one: 1}, {two: 2}, {three: 3})
       */
      join: (...objs: Object[]): object => {
        let ret: Object = {}
        objs.forEach(obj => {
          ret = { ...ret, ...obj }
        })
        return ret
      },
      /**
       * This function is used to transform objects
       * @param {Object} obj - this is the object you want to transform
       * @param {Function} cb - this is a callback for how you want to transform your object
       * @returns {Object} - this is your tranformed object
       */
      transform: (obj: Object, cb: Function) =>
        Object.fromEntries(
          Object.entries(obj).map(([key, val]) => cb(key, val))
        ),
    },
  },
}
