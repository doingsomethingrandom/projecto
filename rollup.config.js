import commonjs from '@rollup/plugin-commonjs'
import nodeResolve from '@rollup/plugin-node-resolve'

const config = {
  input: 'built/index.js',
  output: [
    {
      dir: 'pkg/umd',
      format: 'umd',
      name: 'projectomondo',
    },
    {
      dir: 'pkg/es',
      format: 'es',
      name: 'projectomondo',
    },
    {
      dir: 'pkg/amd',
      format: 'amd',
      name: 'projectomondo',
    },
    {
      dir: 'pkg/cjs',
      format: 'cjs',
      name: 'projectomondo',
    },
    {
      dir: 'pkg/sys',
      format: 'system',
      name: 'projectomondo',
    },
  ],
  plugins: [commonjs(), nodeResolve()],
}

export default config
