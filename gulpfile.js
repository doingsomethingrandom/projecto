const { exec } = require('child_process')
const gulp = require('gulp')
const ts = require('gulp-typescript')
const tsProject = ts.createProject('tsconfig.json')
const del = require('del')
const uglify = require('gulp-uglify')

const execute = cmd =>
  new Promise((_reject, _resolve) =>
    exec(cmd, (error, stdout, stderr) => {
      if (error) {
        console.error(`exec error: ${error}`)
        return
      }
      console.log(`stdout: ${stdout}`)
      console.error(`stderr: ${stderr}`)
    })
  )

gulp.task('transpile', () => {
  return gulp.src('./src/**').pipe(tsProject()).js.pipe(gulp.dest('./built'))
})

gulp.task('package', async () => {
  await execute('npx rollup -c')
  return gulp.src('./pkg/**').pipe(uglify()).pipe(gulp.dest('./dist'))
})

gulp.task('pre-clean', () => {
  return del(['dist'], { force: true })
})

gulp.task('post-clean', () => {
  return del(['built', 'pkg'], { force: true })
})

gulp.task(
  'full-clean',
  gulp.series(gulp.task('pre-clean'), gulp.task('post-clean'))
)

exports.default = gulp.series(
  gulp.parallel('pre-clean', 'transpile'),
  'package',
  'post-clean'
)
