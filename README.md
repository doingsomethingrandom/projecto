# Projectomondo

A lightweight javascript framework

## Install

Run `npm i projectomondo`

## How To Use

To use projectomondo at the start of your file copy and paste this little snippit bellow

```js
const pjmd = require('projectomondo')
```

Now make an awsome app

## How it work

It works by reading `commands.json`

Then parsing it into an object

After that it iterates through that object then evaluating every string

`commands.json` is a json file with lots of arrow functions in strings
