const pjmd = require('projectomondo')

const objA = { a: 'Aa', b: 'Bb' }
const objB = { c: 'Cc', d: 'Dd' }
const objC = { e: 'Ee', f: 'Ff' }

const objABC = pjmd.data.obj.join(objA, objB, objC)
console.log(objABC)

const objABCy = pjmd.data.obj.transform(objABC, (key, val) => [key, val + 'y'])
console.log(objABCy)
