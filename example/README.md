# Projectomondo example

This is a sample app using projectomondo

## Download

Run `git clone https://gitlab.com/doingsomethingrandom/projecto.git`

Use `cd example` to get into this directory

## How to run

First run `npm i`.
Or if you want to use the latest code from the repo run `npm r projectomondo && npm i ../`

Next run `node .` in the `example` directory

Then you should se the output

```bash
{ a: 'Aa', b: 'Bb', c: 'Cc', d: 'Dd', e:'Ee', f: 'Ff' }
{ a: 'Aay', b: 'Bby', c: 'Ccy', d: 'Ddy', e: 'Eey', f: 'Ffy' }
```

## Explanation of how the code works

### `index.js`

---

#### The code

```js
const pjmd = require('projectomondo')

const objA = { a: 'Aa', b: 'Bb' }
const objB = { c: 'Cc', d: 'Dd' }
const objC = { e: 'Ee', f: 'Ff' }

const objABC = pjmd.data.obj.join(objA, objB, objC)
console.log(objABC)

const objABCy = pjmd.data.obj.transform(objABC, (key, val) => [key, val + 'y'])
console.log(objABCy)
```

#### Line by line explanation

1. ```js
   const pjmd = require('projectomondo')
   ```

   This imports `projectomondo` as `pjmd`

2. ```js
   const objA = { a: 'Aa', b: 'Bb' }
   const objB = { c: 'Cc', d: 'Dd' }
   const objC = { e: 'Ee', f: 'Ff' }
   ```

   These are some objects

3. ```js
   const objABC = pjmd.data.obj.join(objA, objB, objC)
   console.log(objABC)
   ```

   This joins `objA` with `objB` and `objC` together and puts it into an object called `objABC`

   Then it outputs it

4. ```js
   const objABCy = pjmd.data.obj.transform(objABC, (key, val) => [
     key,
     val + 'y',
   ])
   console.log(objABCy)
   ```

   This takes `objABC` and add a 'y' to the end of every value in it

   Then it outputs it
